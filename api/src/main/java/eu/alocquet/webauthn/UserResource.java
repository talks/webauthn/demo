package eu.alocquet.webauthn;

import eu.alocquet.webauthn.model.User;
import eu.alocquet.webauthn.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("user")
public class UserResource {

    @Inject
    UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findAll() {
        return userService.findAll();
    }

    @GET
    @Path("/clear")
    public String clearUsers() {
        userService.clear();
        return "<html><head/><body><h1>Users cleared</h1><p><a href=\"/api/user\">users</a></body></html>";
    }

}