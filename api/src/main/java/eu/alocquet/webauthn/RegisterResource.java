package eu.alocquet.webauthn;

import com.webauthn4j.WebAuthnRegistrationManager;
import com.webauthn4j.converter.CollectedClientDataConverter;
import com.webauthn4j.converter.exception.DataConversionException;
import com.webauthn4j.converter.util.ObjectConverter;
import com.webauthn4j.data.PublicKeyCredentialParameters;
import com.webauthn4j.data.PublicKeyCredentialType;
import com.webauthn4j.data.RegistrationParameters;
import com.webauthn4j.data.RegistrationRequest;
import com.webauthn4j.data.attestation.statement.COSEAlgorithmIdentifier;
import com.webauthn4j.data.client.Origin;
import com.webauthn4j.data.client.challenge.DefaultChallenge;
import com.webauthn4j.server.ServerProperty;
import com.webauthn4j.util.Base64UrlUtil;
import com.webauthn4j.util.Base64Util;
import com.webauthn4j.validator.exception.ValidationException;
import eu.alocquet.webauthn.config.WebauthnProperties;
import eu.alocquet.webauthn.model.*;
import eu.alocquet.webauthn.service.SessionService;
import eu.alocquet.webauthn.service.UserService;

import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("register")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RegisterResource {

    @Inject
    UserService userService;

    @Inject
    WebauthnProperties props;

    @Inject
    SessionService session;

    @POST
    @Path("/init")
    public Response init(User user) {
        user.setId(user.getEmail());
        final String challenge = Base64Util.encodeToString(new DefaultChallenge().getValue());
        final Map<String, Object> sessionAttributes = new HashMap<>();
        sessionAttributes.put("CHALLENGE", challenge);
        sessionAttributes.put("USER", JsonbBuilder.create().toJson(user));
        final String sessionId = session.create(sessionAttributes);
        return Response.ok(new RegisterRequest(challenge)).cookie(NewCookie.valueOf("SESSION=" + sessionId)).build();
    }

    @POST
    @Path("/challenge")
    public Response challenge(final Credential<Attestation> body, @CookieParam("SESSION") Cookie cookie) throws Exception {
        var challenge = extractChallenge(body);
        var sessionId = cookie.getValue();

        // retrieve user
        var user = JsonbBuilder.create().fromJson(session.<String>get(sessionId, "USER"), User.class);
        if (user == null) {
            return Response.status(404).build();
        }

        // check challenge
        if (!challenge.equals(session.get(sessionId, "CHALLENGE"))) {
            return Response.status(404).build();
        }

        // Validate Attestation
        var serverProperty = new ServerProperty(
                new Origin(props.origin), // origin url
                props.rpId, // host name
                () -> Base64Util.decode(challenge), // challenge
                new byte[]{}); // token binding id

        var context = new RegistrationRequest(
                Base64Util.decode(body.getResponse().getAttestationObject()),
                Base64Util.decode(body.getResponse().getClientDataJSON()),
                "{}",
                null);

        var registrationParameters = new RegistrationParameters(serverProperty,
                List.of(new PublicKeyCredentialParameters(PublicKeyCredentialType.PUBLIC_KEY, COSEAlgorithmIdentifier.ES256),
                        new PublicKeyCredentialParameters(PublicKeyCredentialType.PUBLIC_KEY, COSEAlgorithmIdentifier.RS1)),
                false, true);

        var webauthnManager = WebAuthnRegistrationManager.createNonStrictWebAuthnRegistrationManager();
        try {
            var registrationData = webauthnManager.parse(context);
            webauthnManager.validate(registrationData, registrationParameters);
        } catch (DataConversionException | ValidationException ex) {
            return Response.status(400).entity(ex.getMessage()).build();
        }

        // persist Authenticator object, which will be used in the authentication process.
        user.setAttestationObject(Base64UrlUtil.encodeToString(Base64Util.decode(body.getResponse().getAttestationObject())));
        userService.create(user);
        session.delete(cookie.getValue());
        return Response.ok(new RegisterChallengeResult()).build();
    }

    private String extractChallenge(Credential<Attestation> body) {
        var clientData = new CollectedClientDataConverter(new ObjectConverter()).convert(body.getResponse().getClientDataJSON());
        return Base64Util.encodeToString(clientData.getChallenge().getValue());
    }

}