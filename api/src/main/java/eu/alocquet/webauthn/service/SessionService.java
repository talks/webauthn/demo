package eu.alocquet.webauthn.service;

import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@ApplicationScoped
public class SessionService {

    @Inject
    Firestore db;

    public String create(final Map<String, Object> sessionAttributes) {
        final String id = UUID.randomUUID().toString();
        db.collection("sessions").document(id).set(sessionAttributes);
        return id;
    }

    public Map<String, Object> get(final String id) {
        try {
            return db.collection("sessions").document(id).get().get().getData();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T> T get(final String id, final String attribute) {
        try {
            return (T)db.collection("sessions").document(id).get().get().getData().get(attribute);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> findAll() {
        try {
            return db.collection("sessions").get().get().getDocuments().stream().map(QueryDocumentSnapshot::getData).collect(Collectors.toList());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void clear() {
        try {
            db.collection("sessions").get().get().getDocuments().forEach(doc -> doc.getReference().delete());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void delete(String sessionId) {
        db.collection("sessions").document(sessionId).delete();
    }
}
