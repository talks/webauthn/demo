package eu.alocquet.webauthn.service;

import com.google.cloud.firestore.Firestore;
import eu.alocquet.webauthn.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@ApplicationScoped
public class UserService {

    @Inject
    Firestore db;

    public void create(final User user) {
        db.collection("users").document(user.getEmail()).set(user);
    }

    public User findByMail(final String mail) {
        try {
            return db.collection("users").document(mail).get().get().toObject(User.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<User> findAll() {
        try {
            return db.collection("users").get().get().toObjects(User.class);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void clear() {
        try {
            db.collection("users").get().get().getDocuments().forEach(doc -> doc.getReference().delete());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
