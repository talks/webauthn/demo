package eu.alocquet.webauthn;

import eu.alocquet.webauthn.service.SessionService;
import eu.alocquet.webauthn.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("session")
public class SessionResource {

    @Inject
    UserService userService;

    @Inject
    SessionService sessionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Map<String, Object>> findAll() {
        return sessionService.findAll();
    }

    @GET
    @Path("/clear")
    public String clearSessions() {
        sessionService.clear();
        return "<html><head/><body><h1>Sessions cleared</h1><p><a href=\"/api/session\">sessions</a></body></html>";
    }

}