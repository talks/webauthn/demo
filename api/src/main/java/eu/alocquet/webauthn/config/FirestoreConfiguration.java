package eu.alocquet.webauthn.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FirestoreConfiguration {

    @ConfigProperty(name = "firestore.mode")
    String mode;

    @ConfigProperty(name = "firestore.file")
    String file;

    @ConfigProperty(name = "firestore.projectId")
    String projectId;

    public void init(@Observes StartupEvent ev) throws IOException {
        if ("local".equals(mode)) {
            initFromJSON();
        } else {
            initOnGCP();
        }
    }

    public void kill(@Observes ShutdownEvent ev) throws IOException {
        FirebaseApp.getApps().forEach(app -> app.delete());
    }

    @Produces
    @Singleton
    public Firestore firestore() throws IOException {
        return FirestoreClient.getFirestore();
    }

    private void initOnGCP() throws IOException {
        // Use the application default credentials
        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .setProjectId(projectId)
                .build();
        FirebaseApp.initializeApp(options);
    }

    private void initFromJSON() throws IOException {
        // Use a service account
        InputStream serviceAccount = new FileInputStream(file);
        GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .build();
        FirebaseApp.initializeApp(options);
    }

}
