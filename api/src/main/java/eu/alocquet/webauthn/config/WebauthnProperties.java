package eu.alocquet.webauthn.config;

import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties(prefix = "webauthn")
public class WebauthnProperties {

    public String rpId;

    public String origin;
}
