package eu.alocquet.webauthn;

import com.webauthn4j.WebAuthnManager;
import com.webauthn4j.authenticator.Authenticator;
import com.webauthn4j.authenticator.AuthenticatorImpl;
import com.webauthn4j.converter.AttestationObjectConverter;
import com.webauthn4j.converter.CollectedClientDataConverter;
import com.webauthn4j.converter.exception.DataConversionException;
import com.webauthn4j.converter.util.ObjectConverter;
import com.webauthn4j.data.AuthenticationData;
import com.webauthn4j.data.AuthenticationParameters;
import com.webauthn4j.data.AuthenticationRequest;
import com.webauthn4j.data.attestation.AttestationObject;
import com.webauthn4j.data.attestation.authenticator.AuthenticatorData;
import com.webauthn4j.data.client.Origin;
import com.webauthn4j.data.client.challenge.DefaultChallenge;
import com.webauthn4j.data.extension.authenticator.RegistrationExtensionAuthenticatorOutput;
import com.webauthn4j.server.ServerProperty;
import com.webauthn4j.util.Base64Util;
import com.webauthn4j.validator.exception.ValidationException;
import eu.alocquet.webauthn.config.WebauthnProperties;
import eu.alocquet.webauthn.model.*;
import eu.alocquet.webauthn.service.SessionService;
import eu.alocquet.webauthn.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Path("authn")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthnResource {

    @Inject
    UserService userService;

    @Inject
    WebauthnProperties props;

    @Inject
    SessionService session;

    @POST
    @Path("init")
    public Response init(final AuthnRequest req) {
        final String challenge = Base64Util.encodeToString(new DefaultChallenge().getValue());
        final Map<String, Object> sessionAtttributes = new HashMap<>();
        sessionAtttributes.put("CHALLENGE", challenge);

        final LoginRequest loginRequest;
        if (req.getEmail() == null) {
            // Passwordless
            loginRequest = new LoginRequestBuilder().setChallenge(challenge).createAuthnOptions();
        } else {
            // with user
            final User user = userService.findByMail(req.getEmail());
            if (user == null) {
                return Response.status(404).build();
            }
            sessionAtttributes.put("USER", user.getEmail());
            loginRequest = new LoginRequestBuilder().setChallenge(challenge).setKeyId(Base64Util.encodeToString(extractAuthenticator(user).getAttestedCredentialData().getCredentialId())).createAuthnOptions();
        }

        final String sessionId = session.create(sessionAtttributes);
        return Response.ok(loginRequest).cookie(NewCookie.valueOf("SESSION=" + sessionId)).build();
    }

    @POST
    public Response challenge(final Credential<Assertion> credential, @CookieParam("SESSION") Cookie cookie) {
        var challenge = extractChallenge(credential);

        var userEmail = Optional
                // from session
                .ofNullable(session.<String>get(cookie.getValue(), "USER"))
                // passwordless case
                .orElseGet(() -> new String(Base64Util.decode(credential.getResponse().getUserHandle())));

        var user = userService.findByMail(userEmail);
        if (user == null) {
            return Response.status(404).build();
        }

        // verify challenge is equal to challenge stored in session
        var challengeFromSession = session.get(cookie.getValue(), "CHALLENGE");
        if (!challenge.equals(challengeFromSession)) {
            return Response.status(400).build();
        }

        // Authentication validation
        var serverProperty = new ServerProperty(
                new Origin(props.origin),
                props.rpId,
                () -> Base64Util.decode(challenge),
                new byte[]{});

        var authenticator = extractAuthenticator(user);

        var authenticationRequest =
                new AuthenticationRequest(
                        Base64Util.decode(credential.getRawId()),
                        Base64Util.decode(credential.getResponse().getAuthenticatorData()),
                        Base64Util.decode(credential.getResponse().getClientDataJSON()),
                        Base64Util.decode(credential.getResponse().getSignature())
                );
        var authenticationParameters =
                new AuthenticationParameters(
                        serverProperty,
                        authenticator,
                        null,
                        false,
                        true
                );


        try {
            var webauthnManager = WebAuthnManager.createNonStrictWebAuthnManager();
            var authenticationData = webauthnManager.parse(authenticationRequest);
            webauthnManager.validate(authenticationData, authenticationParameters);

            updateSignCount(user, authenticationData.getAuthenticatorData().getSignCount());
        } catch (DataConversionException | ValidationException e) {
            return Response.ok(new ChallengeResult(false, e.getMessage())).build();
        }

        return Response.ok(new ChallengeResult(true, user.getName())).build();
    }

    private String extractChallenge(Credential<Assertion> credential) {
        var clientData = new CollectedClientDataConverter(new ObjectConverter()).convert(credential.getResponse().getClientDataJSON());
        return Base64Util.encodeToString(clientData.getChallenge().getValue());
    }

    private Authenticator extractAuthenticator(final User user) {
        var attestation = new AttestationObjectConverter(new ObjectConverter()).convert(user.getAttestationObject());
        return
                new AuthenticatorImpl(
                        attestation.getAuthenticatorData().getAttestedCredentialData(),
                        attestation.getAttestationStatement(),
                        attestation.getAuthenticatorData().getSignCount()
                );
    }

    private void updateSignCount(final User user, long signCount) {
        var attestation = new AttestationObjectConverter(new ObjectConverter()).convert(user.getAttestationObject());
        attestation.getAuthenticatorData().getSignCount();
        user.setAttestationObject(new AttestationObjectConverter(new ObjectConverter()).convertToBase64urlString(
                new AttestationObject(new AuthenticatorData<>(
                        attestation.getAuthenticatorData().getRpIdHash(),
                        attestation.getAuthenticatorData().getFlags(),
                        signCount,
                        attestation.getAuthenticatorData().getAttestedCredentialData(),
                        attestation.getAuthenticatorData().getExtensions()), attestation.getAttestationStatement())));
        userService.create(user);
    }

}