package eu.alocquet.webauthn.model;

public class LoginRequestBuilder {
    private String challenge;
    private String keyId;

    public LoginRequestBuilder setChallenge(String challenge) {
        this.challenge = challenge;
        return this;
    }

    public LoginRequestBuilder setKeyId(String keyId) {
        this.keyId = keyId;
        return this;
    }

    public LoginRequest createAuthnOptions() {
        return new LoginRequest(challenge, keyId);
    }
}