package eu.alocquet.webauthn.model;

public class Attestation extends CredentialResponse {
    private String attestationObject;

    public String getAttestationObject() {
        return this.attestationObject;
    }

    public void setAttestationObject(String attestationObject) {
        this.attestationObject = attestationObject;
    }
}