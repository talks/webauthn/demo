package eu.alocquet.webauthn.model;

public class ChallengeResult {

    private boolean loggedIn = true;

    private String name;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChallengeResult(boolean loggedIn, String name) {
        this.loggedIn = loggedIn;
        this.name = name;
    }
}