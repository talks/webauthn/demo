package eu.alocquet.webauthn.model;

public class CredentialResponse {
    private String clientDataJSON;

    public CredentialResponse() {
    }

    public String getClientDataJSON() {
        return this.clientDataJSON;
    }

    public void setClientDataJSON(String clientDataJSON) {
        this.clientDataJSON = clientDataJSON;
    }
}