package eu.alocquet.webauthn.model;

public class Credential<T extends CredentialResponse> {
    private String rawId;
    T response;
    private String id;
    private String type;

    public Credential() {
    }

    public String getRawId() {
        return this.rawId;
    }

    public T getResponse() {
        return this.response;
    }

    public String getId() {
        return this.id;
    }

    public String getType() {
        return this.type;
    }

    public void setRawId(String rawId) {
        this.rawId = rawId;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

}

