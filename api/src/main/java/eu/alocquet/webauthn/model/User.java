package eu.alocquet.webauthn.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class User {
    private String id;
    private String email;
    private String name;
    private String attestationObject;

    public User() {
    }

    public String getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

    public String getAttestationObject() {
        return attestationObject;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAttestationObject(String attestationObject) {
        this.attestationObject = attestationObject;
    }

}
