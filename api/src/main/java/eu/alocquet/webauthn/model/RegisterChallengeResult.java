package eu.alocquet.webauthn.model;

public class RegisterChallengeResult {
    private boolean registered = true;

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
}