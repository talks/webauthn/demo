// vue.config.js
module.exports = {
    devServer: {
      proxy: {
        "/api/*": {
          target: "http://localhost:3000",
          // target: "https://webauthn.gcp.alocquet.eu",
          secure: false
        }
      }
    }
  };