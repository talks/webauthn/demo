import {parseLoginRequest, parseRegisterRequest} from '@webauthn/server';
import { decodeAllSync } from 'cbor';
import {Unibabel} from "unibabel/index";
const { X509 } = require('jsrsasign');


/**
 * Convert binary certificate or public key to an OpenSSL-compatible PEM text format.
 */
const convertASN1toPEM = pkBuffer => {
    if (!Buffer.isBuffer(pkBuffer)) {
        throw new Error('ASN1toPEM: pkBuffer must be Buffer.');
    }

    let type;
    if (pkBuffer.length === 65 && pkBuffer[0] === 0x04) {
        /*
            If needed, we encode rawpublic key to ASN structure, adding metadata:
            SEQUENCE {
              SEQUENCE {
                 OBJECTIDENTIFIER 1.2.840.10045.2.1 (ecPublicKey)
                 OBJECTIDENTIFIER 1.2.840.10045.3.1.7 (P-256)
              }
              BITSTRING <raw public key>
            }
            Luckily, to do that, we just need to prefix it with constant 26 bytes (metadata is constant).
        */
        pkBuffer = Buffer.concat([
            Buffer.from(
                '3059301306072a8648ce3d020106082a8648ce3d030107034200',
                'hex'
            ),
            pkBuffer,
        ]);

        type = 'PUBLIC KEY';
    } else {
        type = 'CERTIFICATE';
    }

    const b64cert = pkBuffer.toString('base64');

    const PEMKeyMatches = b64cert.match(/.{1,64}/g);

    if (!PEMKeyMatches) {
        throw new Error('Invalid key');
    }

    const PEMKey = PEMKeyMatches.join('\n');

    return `-----BEGIN ${type}-----\n` + PEMKey + `\n-----END ${type}-----\n`;
};

const getCertificateInfo = certificate => {
    const subjectCert = new X509();
    subjectCert.readCertPEM(certificate);

    const subjectString = subjectCert.getSubjectString();
    const subjectParts = subjectString.slice(1).split('/');

    const subject = {};
    for (const field of subjectParts) {
        const kv = field.split('=');
        subject[kv[0]] = kv[1];
    }

    const version = subjectCert.version;
    const basicConstraintsCA = !!subjectCert.getExtBasicConstraints().cA;

    return {
        subject,
        version,
        basicConstraintsCA,
    };
};

const extractCredential = (cred) => {
    const authenticatorKeyBuffer = Buffer.from(
        cred.response.attestationObject,
        'base64'
    );
    const attStmt = decodeAllSync(authenticatorKeyBuffer)[0].attStmt;
    const result = {
        ...cred,
        response: {
            ...cred.response,
            clientDataJSON: JSON.parse(atob(cred.response.clientDataJSON)),
            attestationObject: {
                key: parseRegisterRequest(cred).key,
                x5c: attStmt.x5c ? getCertificateInfo(convertASN1toPEM(attStmt.x5c[0])) : '',
                sig: Unibabel.arrToBase64(attStmt.sig)
            }
        }
    };
    delete result.response.getAuthenticatorData;
    delete result.response.getPublicKey;
    delete result.response.getPublicKeyAlgorithm;
    delete result.response.getTransports;
    delete result.getClientExtensionResults;
    return result;
};
const extractLoginCredential = (cred) => {
    let result =  {
        ...cred,
        response: {
            ...cred.response,
            authenticatorData: cred.response.authenticatorData,
            signature: cred.response.signature,
            userHandle: cred.response.userHandle == null ? null : atob(cred.response.userHandle),
            clientDataJSON: JSON.parse(atob(cred.response.clientDataJSON))
        }
    };
    delete result.getClientExtensionResults;
    return result;
};

export { extractCredential, extractLoginCredential };