import Vue from "vue";

export const store = Vue.observable({
  loggedIn: false,
  name: '',
  debug: false
});

export const mutations = {
    login(name) {
      store.name = name;
      store.loggedIn = true;
    },
    logout() {
      store.name = "";
      store.loggedIn = false;
    },
    change_debug() {
      store.debug = !store.debug;
    }
  };
  