export const CommunicationDirection = {
    KEY_2_BROWSER: 1,
    BROWSER_2_KEY: 2,
    BROWSER_2_SERVER: 3,
    SERVER_2_BROWSER: 4
}